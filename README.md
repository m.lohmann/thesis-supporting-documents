# Lohmann Master Thesis supporting documents

This repository contains files like the source code and movies to the Masters thesis 'Phase-Retrieval via classical optimization algorithms' by Michael Lohmann at the IV. Physical Institute of the Georg-August-Universität Göttingen.
It was supervised by Prof. Claus Ropers, Dr. Sergey Zayko and Dr. Ofer Kfir.


This is supplimentory information for the Masters thesis.
For more information please read the corresponding thesis in which the documents are referenced.

The `sourcecode` uses the python library [ODL](https://github.com/odlgroup/odl) with the commit 2320e39.